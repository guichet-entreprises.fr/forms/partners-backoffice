

log.debug("_INPUT_ : {}", _INPUT_);

var recordUid = nash.record.description().getRecordUid();
log.info("Back to the first page for the record {}", recordUid);
return { url: "/record/" + recordUid + "/1/page/0" };