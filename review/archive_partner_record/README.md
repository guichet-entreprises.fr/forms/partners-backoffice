# Review archived records

The goal is to review archived partner records.

# Teams concerned :
* All referent and partner user for any authority where backoffice channel is enabled.

## First step : Searching authority
First of all, select the target authority
From the roles declared into DIRECTORY, this step checks if the current user is related to the target authority. Otherwise, a warning message will be displayed.

## Second step : Display archived records using criteria

Searching for archived records based on criteria like :
* Any key word : title, description, denomination
* Archiving period 

After research, this step allows to review any archived record.

## Third step : Confirming the review

The record can be read again from the Backoffice dashboard.
