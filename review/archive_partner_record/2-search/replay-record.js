//--------------- Functions --------------//
function replayStorageRecord(author, storageId) {
    log.debug("Replay record for author {} : {}", author, storageId);
    var authorityInfo = getAuthority(author);
    var who = map(authorityInfo, {
        'funcId' : 'entityId', //
        'funcLabel' : 'label' //
    });

    var nfo = authorityInfo.details || undefined;
    log.debug("nfo is  {}", nfo);

    var funcPath = who.funcId;
    if (nfo !== undefined && nfo.parent) {
        funcPath = nfo.parent + '/' + who.funcId;
    }
    log.debug("Authority path : {}", funcPath);
    
    var loadedRecord = nash.instance.from('storage://storage').load(storageId);
    loadedRecord.backToStep(0);
    loadedRecord.role([
        {
            'entity' : funcPath,
            'role' : 'user'
        },
        {
            'entity' : funcPath,
            'role' : 'referent'
        }
    ]);
    var response = loadedRecord.uploadTo("nash://nashBo");
    log.debug('Response upload {}', response);
    
    var newRecordUid = response.asString();
    log.info('Upload new record to nash bo with new identifier {}', newRecordUid);
    
    return newRecordUid;
}

function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}
    
function getAuthority(funcId) {
    var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', funcId) //
        .accept('json') //
        .get();
    // result
    return response.asObject();
}
//--------------- Functions --------------//


//--------------- Variables init --------------//
log.debug("######################## REPLAY RECORD ########################");
log.debug("_input : {}", _input);
var records = _input.record.results.storageRecords.record;
var author = _input.archived.search.authority.id;
//--------------- Variables init --------------//


//--------------- Main --------------//
var url = "/record/" + nash.record.description().getRecordUid() + "/1/page/0";
var newRecordUid = null;
for (var idx = 0; idx < records.size(); idx++) {
    var record = records.get(idx);
    if ("yes" == record.replayButton.value) {
        newRecordUid = replayStorageRecord(author, record.id);
        nash.record.stepsMgr().requestDone(1);
        url = "/record/" + nash.record.description().getRecordUid() + "/2/page/0";
        log.debug("Go to finish step");
        break;
    }
}

var data = nash.instance.load("replay-record.xml");
data.bind('archived.nextStep', {
    'url' : url
});
data.bind('archived.record', {
    'uid' : newRecordUid
});
//--------------- Main --------------//
