log.info("_input : " + _input);
//--------------- Variables init --------------//
var authority = _input.archived.search.authority;
var author = nash.record.description().getAuthor();
var role = "referent";
//--------------- Variables init --------------//

//-->Reset
nash.instance.load("display.xml") //
	.bind("archived.search", {
		"warning" : ""
});

//--------------- Functions --------------//
function hasRole(authority, author, role) {			
	var response = null;
	try {
		response = nash.service.request('${directory.baseUrl}/v1/authority/{authorityFuncId}/user/{userId}/role/{role}', authority.id, author, role) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when calling directory with authority uid : ' + authority.id;
	}
	if (response != null && response.getStatus() == 200) {
		return response.asString();
	}
	throw 'Cannot check rights for authority : ' + authority;
}
//--------------- Functions --------------//

//--------------- Main --------------//
var hasRoleRefent = hasRole(authority, author, role);
log.debug("Access rights to authority {} with role {} : {}", authority, role, hasRoleRefent);

role = "user";
var hasRoleUser = hasRole(authority, author, role);
log.debug("Access rights to authority {} with role {} : {}", authority, role, hasRoleUser);

if ("true" != hasRoleRefent && "true" != hasRoleUser) {
	nash.instance.load("display.xml") //
		.bind("archived.search", {
			'warning' : "<h3 align='center'>Vous ne disposez des droits nécessaires pour " + authority + " !!</h3><h3 align='center'><i class='fa fa-exclamation-triangle fa-4x'></i></h3>"
			
	});
	log.debug("Display warning message ");
}
//--------------- Main --------------//
