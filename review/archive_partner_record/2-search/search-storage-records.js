// try to find the id to search
var keyWord = _input.record.search.keyWord;
var dateSearch = _input.record.search.date;
var traySearch = "partners";
var authority = _input.archived.search.authority;

// Test if a search is needed

if ( authority == null ) {
    return;
}

// --------------------------------------------------------------------------------------
// Find the user
// --------------------------------------------------------------------------------------
function pad(n) {
    return n < 10 ? '0' + n : n;
}

function findRecords(keyWord, dateSearch, traySearch, authority, xmlDisplay, resultsXmlPath) {
    //-->Filters
    var filters = {
        'created>=' : (null != dateSearch && null != dateSearch.from && "" !== dateSearch.from) ? dateSearch.from.getTimeInMillis() : null,
        'created<=' : (null != dateSearch && null != dateSearch.to && "" !== dateSearch.to) ? dateSearch.to.getTimeInMillis() : null
    }
    
    var queryFilter = [];
    
    ['created>=', 'created<='].forEach(function (filter) {
        if (null != filters[filter]) {
            //-->Use pattern yyyy-MM-dd
            log.info('Input date value filter : {}', filters[filter]);
            var created = new Date(parseInt(filters[filter]));
            var month = created.getMonth() + 1;
            var dayOfMonth = created.getDate();
            var createdDateValue = [ //
                created.getFullYear(), //
                ( month > 9 ? '' : '0') + month, //
                ( dayOfMonth >9 ? '' : '0') + (dayOfMonth) //
            ].join('-'); //
            queryFilter.push(filter + createdDateValue);
            log.info('Filtering by {} : {}', filter, createdDateValue);
/*             xmlDisplay.bind(resultsXmlPath + ".input", {
                filter: createdDateValue
            }); */
        }
    });

    //-->Full text search
    searchTerms = [];

        queryFilter.push('tray:' + traySearch);
        log.debug('Filtering by tray : {}', traySearch);
        /* xmlDisplay.bind(resultsXmlPath + ".input", {
            "tray": traySearch
        }); */
    
    if ( keyWord != null) {
//        searchTerms.push(keyWord);
        queryFilter.push('searchTerms:' + keyWord.replace(/\s+/g, '&').replace(/\'/g, '\'\''));
        log.debug('Filtering by keyWord : {}', keyWord);
/*         xmlDisplay.bind(resultsXmlPath + ".input", {
            "keyWord": keyWord
        }) */;
    }
    
    
    if ( authority != null ) {
//        searchTerms.push(authority.id);
        queryFilter.push('searchTerms:' + authority.id);
        log.debug('Filtering by authority author : {}', authority.id);
/*         xmlDisplay.bind(resultsXmlPath + ".input", {
            "authority": authority.getLabel()
        }); */
    }

    fullUrl = _CONFIG_.get('storage.gp.url.ws.private') + '/v1/Record';

    //save call data
    /* xmlDisplay.bind(resultsXmlPath + ".input", {
        "url": fullUrl,
    }); */
    
    try {        
        var nashRequest = nash.service.request(fullUrl) //
            .param('startIndex', 0) //
            .param('maxResults', 100) //
            ;
        
        
        log.info('queryFilter : {}', JSON.stringify(queryFilter));
        queryFilter.forEach(function (filter) {
            nashRequest.param('filters', filter) //
        });
        
        var searchResult = nashRequest //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .param('orders', 'created:desc') //
            .param('orders', 'uid:desc') //
            .get() //
    } catch (error) {
        log.error('=> Error occured while calling storage : ' + error);
        /* xmlDisplay.bind(resultsXmlPath + ".output", {
            "status": "ERROR",
            "response": "" + error,
        }); */
        return null;
    };

    responseObj = searchResult.asObject();
    
/*     xmlDisplay.bind(resultsXmlPath + ".output", {
        "status": searchResult.getStatus(),
        "response": "" + responseObj,
    }); */

    // test the 200 code
    if (searchResult == null || searchResult.getStatus() != 200) {
        return null;
    }

    return responseObj;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user            
var storageRecords = findRecords(keyWord, dateSearch, traySearch, authority, display, "record.debug.storageRecords");

var display = nash.instance.load("display.xml");
display.bind("record.search", { "authority" : "<h2 align='center'> " + authority + " </h2>" });

if (null == storageRecords) {
    display.bind("record.results.storageRecords", { "nbRecords": "0" });
    display.bind("record.results.storageRecords", { "record": [] });
    return; 
}

var displayMetaKeys = ['title', 'description', 'liasse', 'denomination'];
storageRecords.content.forEach(function (record) {
    var metas = [];
    for (var idx in record.metas) {
        var meta = record.metas[idx];
        if (displayMetaKeys.indexOf(meta['key']) != -1 && null != meta['value'] && 'null' != meta['value']) {
            metas.push(meta['value']);
        }
    }
    
    var created = new Date(parseInt(record.created));
    log.debug('record.created : {}', record.created);
    log.debug('created : {}', created);
    var date = created.getDate();
    var month = created.getMonth();
    var year = created.getFullYear();

    record['created'] = '<h5><i>Archivé le ' + date + "/" + pad(month + 1) + "/" + year + '</i></h5>';
    record['metas'] = '<h5><i>Mots-clés : ' + metas.join(', ') + '</i></h5>';
});

display.bind("record.results.storageRecords", { "nbRecords": storageRecords.totalResults });
display.bind("record.results.storageRecords", { "record": storageRecords.content });