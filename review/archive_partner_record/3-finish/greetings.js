
log.debug("_input : " + _input);
var uid = _input.archived.record.uid;

if (null == uid || '' == uid) {
    return ;
}

nash.instance.load("greetings.xml") //
.bind('pageThanks.thanksNew', {
    'description' : 
    "<h4 align='center'>Vous pouvez de nouveau accéder au dossier référencé <a target='_blank' href='/record/" + uid + "'>" + uid + "</a> à partir de votre tableau de bord.</h4>"
});
